<?php
require_once("dbcontroller.php");
$db_handle = new DBController();
$query ="SELECT * FROM state";
$results = $db_handle->runQuery($query);
?>
<html>
<head>
<TITLE>jQuery AJAX dropdown list</TITLE>
<head>
	<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
	<script src="script.js" type="text/javascript"></script>
</head>
<body>
<div class="formDropDown">
<div class="row">
<label>Country:</label><br/>
<select name="country" id="country-list" class="demoInputBox" onChange="getState(this.value);">
<option value="">Select Country</option>
<?php
foreach($results as $country) {
?>
<option value="<?php echo $country["idstate"]; ?>"><?php echo $country["name"]; ?></option>
<?php
}
?>
</select>
</div>
<div id="cities" class="row" style="display: none">
<label>City:</label><br/>
<select name="state" id="state-list" class="demoInputBox">

</select>
</div>
</div>
</body>
</html>