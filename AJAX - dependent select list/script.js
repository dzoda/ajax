
var xmlHttp = createXmlHttpRequestObject();

function createXmlHttpRequestObject() {
  var xmlHttp;

  try {
    xmlHttp = new XMLHttpRequest();
  }
  catch(e) {
    xmlHttp = false;

  }
  return xmlHttp;
}

function getState(val) {

  if(xmlHttp != false) {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == 0) {
      var param = "country_id=" + val;
      xmlHttp.open("POST", "get_city.php");
      xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xmlHttp.send(param);

      xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
          $("#state-list").html(xmlHttp.responseText);
          $("#cities").show("slow");
        }
      };
    }
  }
}

// function getState(val) {
//   $.ajax({
//     type: "POST",
//     url: "get_city.php",
//     data: 'country_id=' + val,
//     success: function (data) {
//       $("#state-list").html(data);
//       $("#cities").show("slow");
//     }
//   });
// }

