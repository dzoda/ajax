-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 13, 2017 at 10:10 AM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gallkster`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `idcity` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `state_idstate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`idcity`, `name`, `state_idstate`) VALUES
(1, 'Novi Sad', 1),
(2, 'Beograd', 1),
(3, 'Moscow', 2),
(4, 'Novorusia', 2),
(5, 'Subotica', 1),
(6, 'Loznica', 1),
(7, 'Dusseldorf', 3),
(8, 'Berlin', 3),
(9, 'Hamburg', 3),
(10, 'Munchen', 3),
(11, 'Frankfurt am Main', 3),
(12, 'Stuttgart', 3),
(13, 'Novosibirsk', 2),
(14, 'Nizhny Novgorod', 2),
(15, 'Athens', 4),
(16, 'Thessaloniki', 4),
(17, 'Veria', 4),
(18, 'Crete', 4),
(19, 'Bratislava', 6),
(20, 'Kosice', 6),
(21, 'Trnava', 6),
(22, 'Nitra', 6);

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `idstate` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`idstate`, `name`) VALUES
(1, 'Serbia'),
(2, 'Russia'),
(3, 'Germany'),
(4, 'Greec'),
(6, 'Slovakia'),
(13, 'Australia'),
(14, 'Austria'),
(15, 'Argentina'),
(16, 'France'),
(17, 'Canada'),
(18, 'Czechia'),
(19, 'Cuba'),
(20, 'China'),
(21, 'Dominican Republic'),
(22, 'Denmark'),
(23, 'Georgia'),
(24, 'Gana'),
(25, 'Jordan'),
(26, 'Jamaica');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `state` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`iduser`, `first_name`, `last_name`, `state`, `city`, `email`, `password`) VALUES
(85, 'Lordan', 'Konstantinovic', '3', '8', 'dzoda@gmail.com', 'abrakadabra'),
(86, 'Gojko', 'Makivic', '1', '1', 'gojkara@gmail.com', 'gojs123'),
(87, 'Petar', 'Vujinovic', '4', '15', 'petar@gmail.com', '123'),
(88, 'Radmila', 'Milinov', '4', '17', 'radmila@rada.com', 'radza');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`idcity`),
  ADD KEY `fk_city_state_idx` (`state_idstate`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`idstate`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `idcity` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `idstate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `fk_city_state` FOREIGN KEY (`state_idstate`) REFERENCES `state` (`idstate`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
