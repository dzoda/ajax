<?php
/**
 * Created by PhpStorm.
 * User: gallkster
 * Date: 10.10.17.
 * Time: 09.30
 */

class DBController {
	private $host = "localhost";
	private $user = "root";
	private $password = "gallkster";
	private $database = "gallkster";
	private $conn;

	function __construct() {
		$this->conn = $this->connectDB();
	}

	function connectDB() {
		$conn = mysqli_connect($this->host,$this->user,$this->password,$this->database);
		return $conn;
	}

	function runQuery($query) {
		$result = mysqli_query($this->conn,$query);
		while($row=mysqli_fetch_assoc($result)) {
			$resultset[] = $row;
		}
		if(!empty($resultset))
			return $resultset;
	}

	function insertQuery($query) {
   mysqli_query($this->conn,$query);
	}

}