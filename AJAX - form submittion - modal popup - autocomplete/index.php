<?php
require_once("dbcontroller.php");
$db_handle = new DBController();
$query ="SELECT * FROM state";
$results = $db_handle->runQuery($query);
?>
<html xmlns="http://www.w3.org/1999/html">
<head>
  <TITLE>jQuery AJAX Driven Form</TITLE>
  <head>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="src/script.js" type="text/javascript"></script>
    <link href="style/style.css" rel="stylesheet" type="text/css" />
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="src/jquery-bootstrap-modal-steps.js"></script>
  </head>
<body>
<div class="frmDronpDown">

  <form id="user" class="userForm"  method="POST">

  <label>First Name:</label>
    <input type="text" name="firstname" id="firstname"> </br></br>
    <label>Last Name:</label>
    <input type="text" name="lastname" id="lastname"> </br></br>
    <label>State:</label>
    <select name="state" id="state" onChange="getState(this.value);">
      <option value="">Select Country</option>
      <?php
      foreach($results as $country) {
        ?>
        <option value="<?php echo $country["idstate"]; ?>"><?php echo $country["name"]; ?></option>
        <?php
      }
      ?>
      </select> </br></br>

    <label>City:</label>
    <select name="city" id="city" >
      <option value="">Select Country</option>

    </select> </br></br>
    <label>E-mail:</label>
    <input type="text" name="email" id="email"> </br></br>
    <label>Password:</label>
    <input type="password" name="password" id="password" onkeyup='check();'></br></br>

    <label>Confirm Password:</label>
    <input type="password" name="cpassword" id="cpassword" onkeyup='check();'> </br></br>

    <label>Contry autocomplete:</label>
    <input type="text" name="auto" id="auto"></br></br>

    <div id="suggesstion-box"></div>
    <input type="submit" name="submit"></br></br>

    <div id="message" class="message"></div>
  </form>


<a href="#"  class="link btn btn-primary btn-lg" onclick="modalInfo()">For more information click here !</a>
  <!--HTML for popup-->
  <div style="display: none;" class="popup-outer">
    <div class="popup-inner">
      <button class="close-popup">&times;</button>
      <!--Popup content-->
      <h2>What is Lorem Ipsum?</h2>
      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
    </div>
  </div>


</div>

<button type="button" class="wizard btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal"> Multi step modal (wizard) </button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="js-title-step"></h4>
      </div>
      <div class="modal-body">
        <div class="row hide" data-step="1" data-title="This is the first step!">
          <div class="well">This is the first step! You can set separate js logic for each modal window.</div>
        </div>
        <div class="row hide" data-step="2" data-title="This is the second and last step!">
          <div class="well">For each modal window you can write custom logic. Logic must be written within callback property .</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default js-btn-step pull-left" data-orientation="cancel" data-dismiss="modal"></button>
        <button type="button" class="btn btn-warning js-btn-step" data-orientation="previous"></button>
        <button type="button" class="btn btn-success js-btn-step" data-orientation="next"></button>
      </div>
    </div>
  </div>
</div>

</body>
</html>