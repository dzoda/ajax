<?php
/**
 * Created by PhpStorm.
 * User: gallkster
 * Date: 11.10.17.
 * Time: 10.37
 */

require_once("userRegistration.php");

$process = new userRegistration();

$user = $process->checkUser();

if (!empty($user)) {
  echo json_encode("202");
}
else {
  $query = $process->query();
  $process->insertQuery($query);
}
