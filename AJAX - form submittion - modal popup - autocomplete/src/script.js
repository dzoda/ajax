$(document).ready(function(){
  // Modal windows. Multi step (wizard).
  $('#myModal').modalSteps({
    btnCancelHtml: 'Cancel',
    btnPreviousHtml: 'Previous',
    btnNextHtml: 'Next',
    btnLastStepHtml: 'Complete',
    disableNextButton: false,
    completeCallback: function(){},
    callbacks: {
      1: function(){
  //Set your validations
        console.log('called step 1');
      },
      2: function(){
        console.log('called step 2');
      },
      3: function(){
        console.log('called step 3');
      }
    }
  });

  // Close simple modal on click (X).
  $(".close-popup").click(function (){
      $(".popup-outer").fadeOut("slow");
    });

  // Autocomplete ajax callback.
  $("#auto").keyup(function(){
    $.ajax({
      type: "POST",
      url: "selectCountry.php",
      data:'keyword='+$(this).val(),
      success: function(data){
        $("#suggesstion-box").show();
        $("#suggesstion-box").html(data);
      }
    });
  });
  // Registration and validation on form submit. User will be inserted into db.
  $("#user").submit(function() {
    var firstname= $("#firstname").val();
    var lastname= $("#lastname").val();
    var country= $("#state").val();
    var city= $("#city").val();
    var email= $("#email").val();
    var password= $("#password").val();
    var message = $('#message');
    var status = check();
    if (firstname == '' || email == '' || password == '' || status != 1 ) {
      message.html('<p class="error">Some fields are required !</p>');
      return false;
    }
    $.ajax({
      type: "POST",
      url: "registration.php",
      data: {firstname:firstname, lastname:lastname, state:country, city:city, email:email, password:password},
      success: function(responseText) {

        if(responseText === '"101"') {
        alert(firstname+" has successefully registrated with email: "+email);
        }
        else if (responseText === '"202"'){
          alert(email+" is already in use ! Try again with another email!");
        }
      }
    });
  });
});

// Function to get cities of specific state.
function getState(val) {
  $.ajax({
    type: "POST",
    url: "get_city.php",
    data:'country_id='+val,
    success: function(data){
      $("#city").html(data);
    }
  });
}

// Function for password validation.
function check() {
  var status;
  if(document.getElementById('cpassword').value !='') {
    if (document.getElementById('password').value ==
      document.getElementById('cpassword').value) {
      document.getElementById('message').style.color = 'green';
      document.getElementById('message').innerHTML = 'Passwords are the same! Correctly';
      status = 1;
    }
    else {
      document.getElementById('message').style.color = 'firebrick';
      document.getElementById('message').innerHTML = 'Password must be the same . Incorrectly';
      status = 0;
    }
  }
  return status;
}

// Select city and hide autocomplete list.
function selectCountry(val) {
  $("#auto").val(val);
  $("#suggesstion-box").hide();
}

// Simple modal trigger.
function modalInfo() {
  $(document).ready(function (){
    $(".popup-outer").fadeIn("slow");
  });
}
