<!DOCTYPE html PUBLIC >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AJAX website with jQuery</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="script.js"></script>
</head>
<body>
<div id="rounded">
<img src="img/top_bg.gif" alt="top" /><div id="main" class="container">
    <h1>AJAX driven jQuery website</h1>
    
    <ul id="navigation">
    <li><a href="#page1">Page 1</a></li>
    <li><a href="#page2">Page 2</a></li>
    <li><a href="#page3">Page 3</a></li>
    <li><a href="#page4">Page 4</a></li>
    </ul>
    
    <div class="clear"></div>
    
    <div id="pageContent">
    Hello, this is a demo. To test it, click some of the buttons above. Have a nice stay!</div>
    
    </div>

</body>
</html>
