<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
  <TITLE>jQuery AJAX</TITLE>
  <head>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="src/script.js" type="text/javascript"></script>
    <link href="style/style.css" rel="stylesheet" type="text/css" />
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

  </head>
<body>

<a href="javascript:void(0);" id="show_form" class="action-button">Show Form</a>

<!-- multistep form -->
<div class="stepsForm">
<form id="msform" method="post">
   progressbar
  <ul id="progressbar">
    <li class="active">About us</li>
    <li>Food order</li>
    <li>Your order</li>
  </ul>
  <!-- fieldsets -->
  <fieldset>
    <h2 class="fs-title">Order your favorite food now !</h2>
    <h3 class="fs-subtitle">Its so easy when you are hungry :)</h3>
    <p>We are here for you . Just populate this form and we will send you a choosen meel.</p>
    <input type="button" name="next" class="next action-button" value="Next" />
  </fieldset>

  <fieldset>
    <h2 class="fs-title">Order form</h2>
    <h3 class="fs-subtitle">Choose your meel</h3>
    <input type="text" name="personal-info" placeholder="First and Last Name" id="personal-info" onchange="getPersonalInfo()"/>
    <p>What would you like for lunch?</p>
    </br>
    <select name="lunch" id="lunch">
      <option value ="none">Nothing</option>
      <option value ="chicken">Chicken</option>
      <option value ="pizza-skulls">Pizza Skulls</option>
      <option value ="papaya">Papaya</option>
      <option value ="mexi">Mexi Ground Beef-Rice Skillet</option>
      </select>
    </br></br>
      <p>Quantity ?</p>
    </br>
      <select name="quantity" id="quantity">
        <option value ="1">1</option>
        <option value ="2">2</option>
        <option value ="3">3</option>
        <option value ="4">4</option>
        <option value ="5">5</option>
    </select>
    </br></br>
    <input type="button" name="previous" class="previous action-button" value="Previous" />
    <input type="button" name="next" class="next action-button" value="Next" />
  </fieldset>
  <fieldset>
    <h2 class="fs-title">Is this your order ? </h2>
    <h3 class="fs-subtitle">Confirm it.</h3>
    <div id="order"> </div>
    <input type="button" name="previous" class="previous action-button" value="Previous" />
    <input type="submit" name="submit" class="submit action-button" value="Submit" />
  </fieldset>
</form>
</div>
<!-- jQuery -->
<!-- jQuery easing plugin -->
<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>

</body>
</html>